# Travel photo classification
This project introduces a solution to the challenge of organizing travel photos for creating personalized travel books. While photography enables travelers to capture memories and share experiences, existing tools often lack automation for efficient photo sorting and organization. To address this gap, this project focuses on developing a classification tool that allows users to categorize their travel photos based on colors or categories. The selected approach involves two key components: color classification using machine learning techniques and category classification using Convolutional Neural Networks. Through extensive experimentation, the most effective combination of features, color spaces, and classifiers for accurate and efficient color classification has been identified.



_See_ **travel_photo_classification_paper.pdf** _for more details._
