import cv2
import numpy as np
import pandas as pd
import glob
import os
import time
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from skimage import feature
from sklearn.pipeline import make_pipeline
from features import *

class Color_classifier:
    def __init__(self, color_space = 'ycbcr', train_dir = "Color_classification_Data/Training"):
        self.color_space = color_space

        self.classifier = make_pipeline(StandardScaler(), SVC(kernel='linear', C=1))
        # self.classifier = make_pipeline(StandardScaler(), KNeighborsClassifier(n_neighbors=2))  # You can adjust the n_neighbors parameter as needed
        
        self.image_paths = glob.glob(train_dir + "/*.*")
        self.labels = [os.path.basename(path).split("_")[0] for path in self.image_paths]
        
        ## Extract features
        self.data = self.extract_features()
        X, y = zip(*self.data)
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(X, y, test_size=0.2, random_state=42)
        self.y_pred = None
        
        self.display_confusion_matrix = False #change to display the confusion matrix


    def extract_features(self):
        """Load images and extract features"""
        
        features = []
        for path, label in zip(self.image_paths, self.labels):
            image = cv2.imread(path)
            
            # Features choice : 
            # hist = color_histogram(image, self.color_space)
            # hist = swain_ballard_histogram(image, self.color_space)
            # hist = local_binary_pattern(image)
            sb_hist = swain_ballard_histogram(image, self.color_space)
            c_hist = color_histogram(image, self.color_space)
            # lbp_hist = local_binary_pattern(image)
            # hist = np.concatenate([sb_hist, lbp_hist])
            # hist = np.concatenate([c_hist, lbp_hist])
            hist = np.concatenate([c_hist, sb_hist])
            
            features.append((hist, label))
    
        return features
   
    def train(self):
        """Train the classifier"""
        
        self.classifier.fit(self.X_train, self.y_train)
        
        self.evaluate()
    
    def evaluate(self):
        """Evaluate the classifier"""
        
        self.y_pred = self.classifier.predict(self.X_test)
        class_labels = np.unique(np.concatenate([self.y_test, self.y_pred]))
        
        cf_matrix = confusion_matrix(self.y_test, self.y_pred, labels=class_labels)
        print("Confusion matrix : ")
        print(cf_matrix)
        if self.display_confusion_matrix:
            plt.figure(figsize=(8, 6))
            sns.heatmap(cf_matrix/np.sum(cf_matrix), annot=True, fmt='.2%', cmap='Greens', xticklabels=class_labels, yticklabels=class_labels)
            plt.xlabel('Predicted Labels')
            plt.ylabel('True Labels')
            plt.title('Confusion Matrix')
            plt.show()

        print("Classification report : ")
        print(classification_report(self.y_test, self.y_pred))
        
        print("Accuracy score : ")
        print(accuracy_score(self.y_test, self.y_pred)) 
    
    def misclassified_images(self):
        """Find misclassified images"""
        
        # Identify misclassified images
        misclassified_indices = np.where(self.y_pred != self.y_test)[0]
        misclassified_paths = np.array(self.image_paths)[misclassified_indices]
        # Print misclassified images
        print("Misclassified Images:")
        for path in misclassified_paths:
            print(path)
            
        return(misclassified_paths)
    
    def classify_image(self, image_path):
        """Use the trained classifier for image classification"""
        
        image = cv2.imread(image_path)
        # Features choice : 
        # hist = swain_ballard_histogram(image, self.color_space)
        # hist = color_histogram(image, self.color_space)
        # hist = local_binary_pattern(image)
        sb_hist = swain_ballard_histogram(image, self.color_space)
        c_hist = color_histogram(image, self.color_space)
        # lbp_hist = local_binary_pattern(image)
        # hist = np.concatenate([sb_hist, lbp_hist])
        # hist = np.concatenate([c_hist, lbp_hist])
        hist = np.concatenate([c_hist, sb_hist])

        
        # hist = self.scaler.transform(hist)
        predicted_label = self.classifier.predict([hist])[0]
        print("the predicted label is: ", predicted_label)
        return predicted_label
    
if __name__ == "__main__":
    start_time = time.time()
    color_classifier = Color_classifier('ycbcr')
    color_classifier.train()
    
    # color_classifier.classify_image("Data/Color/blue/landscape_2799_test.jpg")
    
    ## Time elapsed
    end_time = time.time()
    elapsed_time = end_time - start_time
    print(f"Time elapsed: {elapsed_time} seconds")

    