import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os
import json
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications import VGG16
from tensorflow.keras import layers
from tensorflow.keras import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing import image
from tensorflow.keras.models import load_model
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

class Category_classifier:
    def __init__(self, train_dir = 'Data/Training', validation_dir = 'Data/Validation', test_dir = 'Data/Test', class_labels = ['Food', 'Landmarks', 'Landscapes'], batch_size=32, epochs=10):
        self.train_dir = train_dir
        self.validation_dir = validation_dir
        self.test_dir = test_dir
        self.class_labels = class_labels
        
        self.batch_size = batch_size
        self.epochs = epochs
        
        self.train_generator, self.validation_generator, self.test_generator = self.create_data_generators()
        self.model = self.create_model()
        
        self.test_loss = None
        self.test_accuracy = None
        self.predicted_classes = None
        self.true_classes = None
        self.cf_matrix = None
        self.classification_report = None
        self.misclassified_info = None

    def create_data_generators(self):
        """Create the train, validation and test data generators"""   
          
        train_datagen = ImageDataGenerator(rescale=1./255)
        train_generator = train_datagen.flow_from_directory(
            self.train_dir,
            target_size=(224, 224),
            batch_size=self.batch_size,
            class_mode='categorical'
        )

        validation_datagen = ImageDataGenerator(rescale=1./255)
        validation_generator = validation_datagen.flow_from_directory(
            self.validation_dir,
            target_size=(224, 224),
            batch_size=self.batch_size,
            class_mode='categorical'
        )

        test_datagen = ImageDataGenerator(rescale=1./255)
        test_generator = test_datagen.flow_from_directory(
            self.test_dir,
            target_size=(224, 224),
            batch_size=self.batch_size,
            class_mode='categorical',
            shuffle=False  # Keep the order of images for evaluation
        )

        print("Number of test samples:", test_generator.samples)
        print("Batch size:", self.batch_size)

        return(train_generator, validation_generator, test_generator)
    
    def create_model(self):
        """Creat the model, using a pre-trained VGG16 model and modifying the output layer"""
        
        ## Load pre-trained VGG16 model
        base_model = VGG16(weights='imagenet', include_top=False, input_shape=(224, 224, 3))

        # Freeze convolutional layers
        for layer in base_model.layers:
            layer.trainable = False
        
        ## Add custom output layer for classification task
        x = base_model.output
        x = layers.GlobalAveragePooling2D()(x)
        x = layers.Dense(256, activation='relu')(x)
        x = layers.Dropout(0.5)(x)
        predictions = layers.Dense(len(self.class_labels), activation='softmax')(x)

        model = Model(inputs=base_model.input, outputs=predictions)

        ## Compile the model
        model.compile(optimizer=Adam(), loss='categorical_crossentropy', metrics=['accuracy'])

        return model
        
    def train(self):
        """Train the model"""
        
        history = self.model.fit(
        self.train_generator,
        steps_per_epoch=self.train_generator.samples // self.batch_size,
        epochs=self.epochs,
        validation_data=self.validation_generator,
        validation_steps=self.validation_generator.samples // self.batch_size
        )
        
        # Evaluate the model and identify misclassifid images
        self.evaluate()
        self.misclassified_images()

        # Save the trained model and evaluation
        self.save_model()

    def save_model(self, model_path='models/trained_model.h5'):
        """Save the model into a .h5 file, at the path indicated by model_path"""
        
        self.model.save(model_path)

    def load_model(self, model_path='models/trained_model.h5'):
        """Load the model from the .h5 file model_path"""
        
        if os.path.exists(model_path):
            print(f"Loading pre-trained model from {model_path}")
            self.model = load_model(model_path)
            self.evaluate()
            self.misclassified_images()
            
            model_loaded = True
        else:
            print(f"No pre-trained model found at {model_path}. Try training a new model.")
            model_loaded = False
            
        return model_loaded

    def evaluate(self):
        """Evaluate the model with the accuracy, the confusion matrix and the classification report"""
        
        self.test_loss, self.test_accuracy = self.model.evaluate(self.test_generator)
        print(f'Test accuracy: {self.test_accuracy}')

        predictions = self.model.predict(self.test_generator)
        self.predicted_classes = predictions.argmax(axis=1)
        self.true_classes = self.test_generator.classes
        
        self.cf_matrix = confusion_matrix(self.true_classes, self.predicted_classes)
        print("Confusion matrix :\n", self.cf_matrix)
        plt.figure(figsize=(8, 6))
        sns.heatmap(self.cf_matrix/np.sum(self.cf_matrix), annot=True, fmt='.2%', cmap='Greens', 
                    xticklabels=self.class_labels, yticklabels=self.class_labels)
        plt.xlabel('Predicted Labels')
        plt.ylabel('True Labels')
        plt.title('Confusion Matrix')
        plt.show()

        self.classification_report = classification_report(self.true_classes, self.predicted_classes, target_names=self.class_labels)
        print("Classification report :\n", self.classification_report)
        
        return(self.test_accuracy, self.cf_matrix, self.classification_report)
    
    def misclassified_images(self):
        """Identify misclassified images. Returns their path and their true and predicted classes"""
        
        misclassified_indices = np.where(self.predicted_classes != self.true_classes)[0]

        self.misclassified_info = []
        for index in misclassified_indices:
            img_path = self.test_generator.filepaths[index]
            true_class_index = self.test_generator.classes[index]
            predicted_class_index = self.predicted_classes[index]
            
            true_class = self.class_labels[true_class_index]
            predicted_class = self.class_labels[predicted_class_index]
        
            self.misclassified_info.append({
                'path': img_path,
                'true_class': true_class,
                'predicted_class': predicted_class
            })

        for info in self.misclassified_info:
            print(f"Path: {info['path']}, True Class: {info['true_class']}, Predicted Class: {info['predicted_class']}")
        
        return self.misclassified_info

    def predict_image(self, image_path):
        """Predict the class of image_path"""
    
        # Load an individual image for prediction
        img = image.load_img(image_path, target_size=(224, 224))
        img_array = image.img_to_array(img)
        img_array = np.expand_dims(img_array, axis=0)
        img_array /= 255.  # Normalize pixel values to between 0 and 1, same as during training
        
        # Make predictions
        predictions = self.model.predict(img_array)
        predicted_class_index = np.argmax(predictions[0])
        predicted_class = self.class_labels[predicted_class_index]

        print(f"Predicted class: {predicted_class}")
        return predicted_class

if __name__ == "__main__":
    category_classifier = Category_classifier()
    model_loaded = category_classifier.load_model()

    if not model_loaded:
        model = category_classifier.create_model()
        category_classifier.train(model)
    
    category_classifier.predict_image('Data/Test/Food/food_129_test.jpg')
    category_classifier.predict_image('Data/Test/Food/food_1980_test.jpg')

