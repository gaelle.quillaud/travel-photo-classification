import cv2
import numpy as np
from skimage import feature

## Color histogram functions, for different color spaces
def rgb_color_histogram(image):
    """Extract color histograms from the rgb image"""
    
    # Calculate histograms for each channel
    hist_r = cv2.calcHist([image], [0], None, [256], [0, 256])
    hist_g = cv2.calcHist([image], [1], None, [256], [0, 256])
    hist_b = cv2.calcHist([image], [2], None, [256], [0, 256])
    
    # Concatenate histograms into a single feature vector
    hist = np.concatenate([hist_r, hist_g, hist_b]).flatten()
    
    return hist

def hsv_color_histogram(image):
    """Extract color histograms from the hsv image"""
    
    # Convert image to HSV color space
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    
    # Calculate histograms for each channel
    hist_hue = cv2.calcHist([hsv_image], [0], None, [256], [0, 256])
    hist_saturation = cv2.calcHist([hsv_image], [1], None, [256], [0, 256])
    hist_value = cv2.calcHist([hsv_image], [2], None, [256], [0, 256])
    
    # Concatenate histograms into a single feature vector
    hist = np.concatenate([hist_hue, hist_saturation, hist_value]).flatten()
    
    return hist

def lab_color_histogram(image):
    """Extract color histograms from the lab image"""
    
    # Convert image to Lab color space
    lab_image = cv2.cvtColor(image, cv2.COLOR_BGR2Lab)
    
    # Calculate histograms for each channel
    hist_l = cv2.calcHist([lab_image], [0], None, [256], [0, 256])
    hist_a = cv2.calcHist([lab_image], [1], None, [256], [0, 256])
    hist_b = cv2.calcHist([lab_image], [2], None, [256], [0, 256])
    
    # Concatenate histograms into a single feature vector
    hist = np.concatenate([hist_l, hist_a, hist_b]).flatten()
    
    return hist

def ycbcr_color_histogram(image):
    """Extract color histograms from the lab image"""
    
    # Convert image to Lab color space
    ycbcr_image = cv2.cvtColor(image, cv2.COLOR_BGR2YCrCb)
    
    # Calculate histograms for each channel
    hist_y = cv2.calcHist([ycbcr_image], [0], None, [256], [0, 256])
    hist_cb = cv2.calcHist([ycbcr_image], [1], None, [256], [0, 256])
    hist_cr = cv2.calcHist([ycbcr_image], [2], None, [256], [0, 256])
    
    # Concatenate histograms into a single feature vector
    hist = np.concatenate([hist_y, hist_cb, hist_cr]).flatten()
    
    return hist

def color_histogram(image, color_space = 'ycbcr'):
    """Appropriate function for the color space chosen"""

    color_space_functions = {
        'hsv': hsv_color_histogram,
        'rgb': rgb_color_histogram,
        'lab': lab_color_histogram,
        'ycbcr': ycbcr_color_histogram,
    }
    color_histogram_function = color_space_functions.get(color_space, ycbcr_color_histogram)
    return color_histogram_function(image)

## Swain & Ballard histogram functions, for different color spaces 
def rgb_swain_ballard_histogram(image):
    """Extract Swain & Ballard histogram from the rgb image"""

    hist = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8], [0, 180, 0, 256, 0, 256])
    hist = cv2.normalize(hist, hist).flatten()
    return hist

def hsv_swain_ballard_histogram(image):
    """Extract Swain & Ballard histogram from the hsv image"""

    # Convert image to HSV color space
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    # Calculate histogram
    hist = cv2.calcHist([hsv_image], [0, 1, 2], None, [8, 8, 8], [0, 180, 0, 256, 0, 256])
    hist = cv2.normalize(hist, hist).flatten()
    return hist

def lab_swain_ballard_histogram(image):
    """Extract Swain & Ballard histogram from the lab image"""

    # Convert image to Lab color space
    lab_image = cv2.cvtColor(image, cv2.COLOR_BGR2Lab)

    # Calculate histogram
    hist = cv2.calcHist([lab_image], [0, 1, 2], None, [8, 8, 8], [0, 180, 0, 256, 0, 256])
    hist = cv2.normalize(hist, hist).flatten()
    return hist

def ycbcr_swain_ballard_histogram(image):
    """Extract Swain & Ballard histogram from the lab image"""

    # Convert image to Lab color space
    ycbcr_image = cv2.cvtColor(image, cv2.COLOR_BGR2YCrCb)

    # Calculate histogram
    hist = cv2.calcHist([ycbcr_image], [0, 1, 2], None, [8, 8, 8], [0, 180, 0, 256, 0, 256])
    hist = cv2.normalize(hist, hist).flatten()
    return hist

def swain_ballard_histogram(image, color_space = 'ycbcr'):
    """Appropriate function for the color space chosen"""

    color_space_functions = {
        'hsv': hsv_swain_ballard_histogram,
        'rgb': rgb_swain_ballard_histogram,
        'lab': lab_swain_ballard_histogram,
        'ycbcr': ycbcr_swain_ballard_histogram,
    }
    sb_histogram_function = color_space_functions.get(color_space, ycbcr_swain_ballard_histogram)
    return sb_histogram_function(image)

## Local binary pattern function
def local_binary_pattern(image, num_points=24, radius=8):
    """Extract the LBP histogram from the image"""
    
    # Convert image to grayscale
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Calculate histogram
    lbp = feature.local_binary_pattern(gray_image, num_points, radius, method="uniform")
    hist, _ = np.histogram(lbp.ravel(), bins=np.arange(0, num_points + 3), range=(0, num_points + 2))
    hist = hist.astype("float")
    hist /= (hist.sum() + 1e-7)
    
    return hist
